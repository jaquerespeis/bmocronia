# BMOCronía

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat)](https://github.com/RichardLitt/standard-readme)

El bot de Radiocronía. También conocido como BMOCronía.

## Instalación

En cualquiera de las [distribuciones de Linux soportadas](https://snapcraft.io/docs/core/install):

```
sudo snap install bmocronia --edge
```

## Uso

```
BMOCRONIA_TELEGRAM_TOKEN=${BMOCRONIA_TELEGRAM_TOKEN} bmocronia &
```

Donde `${BMOCRONIA_TELEGRAM_TOKEN}` es el token del bot de Telegram.

## Mantenedor

[@elopio](https://gitlab.com/elopio/)

## Contribuir

Si quieren contribuir, escriban en el [bunqueer](https://bunqueer.jaquerespeis.org/t/chatbot-para-controlar-liquidsoap/1085) o abran un [issue](https://gitlab.com/jaquerespeis/bmocronia/-/issues/new).

## License

[GNU General Public License v3.0 or later](LICENSE) (C) 2020 JáquerEspeis
