import os
import logging


BACKEND = 'Telegram'

TELEGRAM_ID_ELOPIO = '43624396'

BOT_ADMINS = (TELEGRAM_ID_ELOPIO,)

BOT_IDENTITY = {
    'token': os.environ.get('BMOCRONIA_TELEGRAM_TOKEN'),
}

CHATROOM_PRESENCE = ()
BOT_PREFIX = '/'
CORE_PLUGINS = ()

BOT_DATA_DIR = os.environ.get('SNAP_USER_DATA')
BOT_EXTRA_PLUGIN_DIR = os.path.join(os.environ.get('SNAP'), 'plugins')

BOT_LOG_LEVEL = logging.DEBUG
BOT_LOG_FILE = BOT_DATA_DIR + '/err.log'
